package com.example.mybeastapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.mybeastapp.AdapterObjects.Profile
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.ml.naturallanguage.FirebaseNaturalLanguage
import com.google.firebase.ml.naturallanguage.languageid.FirebaseLanguageIdentification
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {



    var TAG:String = "MainActivity"
    //DECLARE = INITIALIZE
    lateinit var auth:FirebaseAuth
    var db = FirebaseFirestore.getInstance()
    private lateinit var remoteConfig: FirebaseRemoteConfig

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        remoteConfig=Firebase.remoteConfig
        val configSettings:FirebaseRemoteConfigSettings = remoteConfigSettings { this
            minimumFetchIntervalInSeconds=3600
        }
        remoteConfig.setConfigSettingsAsync(configSettings)
        //remoteConfig.setDefaultsAsync(R.Xml.remote_config_defaults)

        auth = FirebaseAuth.getInstance()
        clRegistro.visibility=View.GONE
        clLogin.visibility=View.VISIBLE

        //auth.signOut()

        if (auth.currentUser!=null){
            val intent = Intent(this, careScrollingActivity:: class.java)
            startActivity(intent)
            finish()

        }


    }

    fun onClicklogin(v:View){
        var sEmail:String=etEmail.text.toString()
        var sPassword:String=etpassword.text.toString()

        auth.signInWithEmailAndPassword(sEmail, sPassword)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "SingInWithEmail:success")

                    val user = auth.currentUser


                    val intent = Intent(this, careScrollingActivity:: class.java)
                    startActivity(intent)
                    finish()
                    //Toast.makeText(baseContext, "Login Successefull.",
                    //    Toast.LENGTH_SHORT).show()
                    //updateUI(user)

                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "singInWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                    //updateUI(null)
                }

                // ...
            }




          }
    fun onClickSingup(v:View){
        clLogin.visibility=View.GONE
        clRegistro.visibility=View.VISIBLE

    }

    fun onClickConfirm(v:View){
        var sEmailSingUp:String=etEmailSingUp.text.toString()
        var sPassSingUp:String=etPassSingup.text.toString()
        var sRepassSingUp=etRePassSingUp.text.toString()

        if(sPassSingUp==sRepassSingUp){
            auth.createUserWithEmailAndPassword(sEmailSingUp,sPassSingUp)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "createUserWithEmail:success")
                        val user = auth.currentUser

                        val Profile= Profile()
                        Profile.modelo=editText_Name.text.toString()
                        Profile.precio=editText_Age.text.toString().toInt()
                        Profile.Imagen="https://www.elconfidencialdigital.com/asset/zoomcrop%252C1366%252C800%252Ccenter%252Ccenter//media/elconfidencialdigital/images/2018/01/29/ECDIMA20180129_0002_1.png"


                        db.collection("Profiles").document(user!!.uid)
                            .set(Profile)
                            .addOnSuccessListener { }
                            .addOnFailureListener { }
                        //updateUI(user)
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "createUserWithEmail:failure", task.exception)
                        Toast.makeText(baseContext, "Authentication failed.",
                            Toast.LENGTH_SHORT).show()
                        //updateUI(null)
                    }

                    // ...
                }








        }
        else{
            Toast.makeText(baseContext, "The password are not equal",Toast.LENGTH_SHORT).show()

        }
    }

    fun onClickCancel(v:View){
        clRegistro.visibility=View.GONE
        clLogin.visibility=View.VISIBLE


    }
    fun identifyLanguage(inputText:String){
        val languageIdentification = FirebaseNaturalLanguage.getInstance().languageIdentification

        languageIdentification.identifyLanguage(inputText).addOnSuccessListener { s -> outputText?.append(String.format(Locale.US,"\n%s - %s",inputText,s)


        )

        }

            .addOnFailureListener{e ->
                Log.e(TAG,"Language identification error.",e)

            }
    }
    fun identifyPossibleLanguage(inputText:String){
        val languageIdentification :FirebaseLanguageIdentification = FirebaseNaturalLanguage.getInstance()
            .languageIdentification

        languageIdentification.identifyPossibleLanguages(inputText)
            .addOnSuccessListener {indentifyLanguages ->
                val detectedLanguages = ArrayList<String>(indentifyLanguages.size)

                for (language in indentifyLanguages ){
                    detectedLanguages.add(
                        String.format(Locale.US, "%s (%3f)",language.languageCode, language.confidence))
                }

            }

    }
}




