package com.example.mybeastapp.celdas

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mybeastapp.AdapterObjects.Profile
import com.example.mybeastapp.R
import com.squareup.picasso.Picasso


class Micelda1(v: View) : RecyclerView.ViewHolder(v){

    private var root:View=v

    fun bindData(dato:Profile) {
        val micelda_image: ImageView = root.findViewById(R.id.micelda_image)
        val micelda_text: TextView = root.findViewById(R.id.micelda_text)
        val micelda_precio: TextView = root.findViewById(R.id.micelda_precio)


        micelda_text.text = dato.modelo
        micelda_precio.text = dato.precio.toString()
        if (!dato.Imagen.isEmpty()) {
            Picasso.get().load(dato.Imagen).into(micelda_image)

            /*micelda_image.setImageResource(dato.avatar!!)
        var txtcursos=""
        for (curso in dato.curso!!){
            txtcursos=txtcursos+"\n"+curso

        }
        micelda_curso.text=txtcursos*/

        }

    }


}