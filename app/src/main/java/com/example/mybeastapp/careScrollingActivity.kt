package com.example.mybeastapp

import android.os.Bundle
import android.view.View
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import com.example.mybeastapp.Fragments.FirstFragment
import com.example.mybeastapp.Fragments.ListFragment
import com.example.mybeastapp.Fragments.SecondFragment
import kotlinx.android.synthetic.main.activity_care_scrolling.*

class careScrollingActivity : AppCompatActivity() {

    var fragmentFirts: FirstFragment? = null
    var fragmentSecond: SecondFragment? = null
    var fragmentList: ListFragment? = null


    var blShowingFirts:Boolean=false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_care_scrolling)
        setSupportActionBar(toolbar)

        fragmentFirts = FirstFragment()
        fragmentSecond = SecondFragment()
        fragmentList = ListFragment()


        //showFirtsFragment()
        ShowListFragment()

    }

    fun onClickFab(v: View) {

        if(blShowingFirts){
            ShowListFragment()
        }
        else{
            showFirtsFragment()

        }

    }

    fun showFirtsFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.FlContainer, fragmentFirts!!)
        transaction.commit()
        blShowingFirts=true

    }

    /*fun ShowSecondFragment() {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.FlContainer, fragmentSecond!!)
        transaction.commit()
        blShowingFirts=false

    }
*/


    fun  ShowListFragment(){
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.FlContainer, fragmentList!!)
        transaction.commit()
        blShowingFirts=false

    }
}


