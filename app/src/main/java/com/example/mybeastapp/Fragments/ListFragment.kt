package com.example.mybeastapp.Fragments

import adapters.Liat2adapter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mybeastapp.AdapterObjects.Profile
import com.example.mybeastapp.AdapterObjects.StudentsItem
import com.example.mybeastapp.R
import com.google.firebase.firestore.FirebaseFirestore


/**
 * A simple [Fragment] subclass.
 */
class ListFragment : Fragment(), View.OnClickListener{

     lateinit var rv_list:RecyclerView
    var db = FirebaseFirestore.getInstance()
    lateinit var btnInsertar:Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root:View = inflater.inflate(R.layout.fragment_list, container, false)

        rv_list=root.findViewById(R.id.rv_list)
        btnInsertar=root.findViewById(R.id.btnInsertar)
        btnInsertar.setOnClickListener(this)




        rv_list.layoutManager = LinearLayoutManager(context)
       // rv_list!!.layoutManager = GridLayoutManager(context,spanCount:1)



        /*val arStudentsClase= arrayListOf<StudentsItem>()

        val Mario:StudentsItem= StudentsItem("H",20,
            arrayListOf("Advanced Programing","Technology Innovation"),"Mario")
        Mario.loadImg(R.drawable.img4)


        val Alvaro:StudentsItem= StudentsItem("H",20,
            arrayListOf("Advanced Programing","Estadistica"),"Alvaro")
        Alvaro.loadImg(R.drawable.img1)

        val Blanca:StudentsItem= StudentsItem("M",19,
            arrayListOf("Inglés","Proyecto de Empresa"),"Blanca")
        Blanca.loadImg(R.drawable.img3)

        val Pepe:StudentsItem= StudentsItem("H",21,
            arrayListOf("Inglés","Matemáticas"),"Pepe")
        Pepe.loadImg(R.drawable.img2)


        arStudentsClase.add(Mario)
        arStudentsClase.add(Alvaro)
        arStudentsClase.add(Blanca)
        arStudentsClase.add(Pepe)*/



        var arProfiles= arrayListOf<Profile>()

       /*db.collection("Profiles").get()
            .addOnSuccessListener { result ->

                val listaDescargada=result.toObjects(Profile::class.java)
                arProfiles.addAll(listaDescargada)
                val adapter: Liat2adapter= Liat2adapter(arProfiles, context!!)
                rv_list.adapter=adapter
            }
            .addOnFailureListener { exception ->
                Log.v("ListFragment2","--->>>>>    "+exception)


            }*/

        val docRef = db.collection("Profiles")
        docRef.addSnapshotListener { snapshot, e ->
            if (e != null) {
                Log.w("ListFragment2", "Listen failed.", e)
                return@addSnapshotListener
            }
            /*en caso de error se usa lo de arriba*/

            if (snapshot != null) {
                arProfiles = arrayListOf()
                /*limpiador de problemas*/
                val listaDescargada=snapshot.toObjects(Profile::class.java)
                arProfiles.addAll(listaDescargada)
                val adapter: Liat2adapter= Liat2adapter(arProfiles, context!!)
                rv_list.adapter=adapter

                Log.d("ListFragment2", "Current data: ${snapshot.documents}")
            } else {
                Log.d("ListFragment2", "Current data: null")
            }
        }


        return root
    }

    override fun onClick(p0: View?) {
        if (p0==btnInsertar){
           /* val Profile = hashMapOf(
                "name" to "Pablo",
                "age" to "21".toInt(),
                "avatar" to "https://i.imgur.com/uB06KEm.png"
            )*/
            val Profile=Profile()
            Profile.modelo="Sustitucion"
            Profile.precio=0
            Profile.Imagen="https://www.elconfidencialdigital.com/asset/zoomcrop%252C1366%252C800%252Ccenter%252Ccenter//media/elconfidencialdigital/images/2018/01/29/ECDIMA20180129_0002_1.png"




            db.collection("Profiles")
                .add(Profile)
                .addOnSuccessListener { }
                .addOnFailureListener { }

        }

    }

}
