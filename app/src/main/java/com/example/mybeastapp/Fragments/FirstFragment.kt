package com.example.mybeastapp.Fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton

import com.example.mybeastapp.R
import com.google.android.material.snackbar.Snackbar

/**
 * A simple [Fragment] subclass.
 */
class FirstFragment : Fragment(), View.OnClickListener{


    var btn1:Button?=null
    var btn2:Button?=null
    var imageButton:ImageButton?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root:View = inflater.inflate(R.layout.fragment_first, container, false)
        btn1=root.findViewById(R.id.button5)
        btn2=root.findViewById(R.id.button6)
        imageButton=root.findViewById(R.id.imageButton2)

        btn1!!.setOnClickListener(this)
        btn2!!.setOnClickListener(this)
        imageButton!!.setOnClickListener(this)




        return root
    }
    override fun onClick(p0: View?) {
        if(p0!!.id==btn1!!.id){
            Snackbar.make(p0, R.string.snacbar_msg1, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()

        }
        else if(p0!!.id==btn2!!.id){
            Snackbar.make(p0, "Replace with your own action btn2", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()

        }
        else if (p0!!.id==imageButton!!.id){
            Snackbar.make(p0, "JUST CLICKED THE UCJC ICON!!", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

    }



}
