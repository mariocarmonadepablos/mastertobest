package adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mybeastapp.AdapterObjects.Profile
import com.example.mybeastapp.AdapterObjects.StudentsItem
import com.example.mybeastapp.R
import com.example.mybeastapp.celdas.Micelda1

class Liat2adapter:RecyclerView.Adapter<Micelda1>{

    var  arData:ArrayList<Profile>?=null
    var context:Context?=null

    constructor(arData:ArrayList<Profile>,context:Context ){
        this.arData=arData
        this.context=context

    }

    /**
     * Function that assigns the specific data from ARDATA in the position "position" to
     *the cell "celda" that the system wants to show.It will execute X (arData.size)
     * @param celda is the cell that the system gives us to use at the moment of painting
     * @param position is the position of de cell (i.e. position 25) which tells us that the
     * system is painting the cell at position 25, so we access ARDATA [25]
     */
    override fun onBindViewHolder(celda: Micelda1, position: Int) {
        celda.bindData(arData!!.get(position))

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Micelda1 {

        val root: View = LayoutInflater.from(context).inflate(R.layout.micelda_layout, parent, false)
        //root.layoutParams.height=400
        val micelda1:Micelda1= Micelda1(root)

        return micelda1

    }

    override fun getItemCount(): Int {
        return arData!!.size
    }

}