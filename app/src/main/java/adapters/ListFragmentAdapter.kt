package adapters

import android.content.Context
import android.provider.ContactsContract
import android.util.Log
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mybeastapp.R

class ListFragmentAdapter:RecyclerView.Adapter<ListElement>{

    var arNames:ArrayList<String>?=null
    var context:Context?=null


    constructor(arNames:ArrayList<String>,context:Context){
        this.arNames=arNames
        this.context=context

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListElement {
        val root:View = LayoutInflater.from(context).inflate(R.layout.name_list_item, parent, false)
        var listElement:ListElement=ListElement(root)
        return listElement

    }

    override fun getItemCount(): Int {
        return this.arNames!!.size

    }

    override fun onBindViewHolder(holder: ListElement, position: Int) {
        holder.bindData(this.arNames!!.get(position))


    }
}

class ListElement(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {

    private var view: View = v

    init {
        v.setOnClickListener(this)
    }
    fun bindData(data: String){
        val tvName:TextView=view.findViewById(R.id.tvName)
        tvName.setText(data)

    }

    override fun onClick(v: View) {
        Log.d("RecyclerView", "CLICK!")
    }

}